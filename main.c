#include "main.h"
#include <stdlib.h>

// Amethyst Skye // CSE222 // 1-11-2021 //
// Programming Assignment 1 //
// A program written in C which allows the user to interact with a linked list.
// The user will be prompted upon executing the program.
// Command options for interacting with the list include:
// i number - inserts a number into the list, placing it in order from 
// smallest to largest.
// p - will print the list in order, on one line separated by spaces.
// s number - searches for the given number and prints "FOUND" or "NOT FOUND".
// d number - deletes the node containing the given number and prints "SUCCESS" or "NODE NOT FOUND".
// x - exit program
// Any other input - a synopsis of legal commands will be printed.
// After a command is executed, the program will prompt the user for another command.

int main()
{
struct node ll[100]; //initialize 100 elements of type struct node
char str[120]; //for user input
char input; //command interactions
int num; //number given by user
init(ll); //initialize linked list

//until user types 'x' to exit program
while(input != 'x'){
  printf(">"); //user prompt
  fgets(str,120,stdin);
  sscanf(str,"%c" "%d",&input, &num); //recieves input and num as command

//i - insert command//
    if(input == 'i'){ 
     // add(ll,num); //use add function
      int reti=add(ll,num); //return value from add
      if(reti == 1){ //data added successfully
	printf("SUCCESS\n");
      }
    }
//p - print command//
    else if(input == 'p'){
      print(ll); //print linked list with a space in between each entry
    }
//s number - search command//
    else if(input == 's'){
      int rets=search(ll,num); //saves return value of search
      if(rets == 1){
        printf("FOUND\n");
      }
      else if(rets == 0){
        printf("NOT FOUND\n");
      }
    }
//d number - delete command//
    else if(input == 'd'){
      int retd=delete(ll,num); //return value of delete function
      if(retd == 1){ //successful delete
        printf("SUCCESS\n");
      }
      else if(num != search(ll,num)){
        printf("NODE NOT FOUND\n");
      }
    }
//x - exit command//
    else if(input == 'x'){
      exit(0);
    }
//everything else//
//prints a statement which lists all possible command entries
    else{
      printf("Please use one of the following commands:\n");
      printf("i number - inserts data to list\n");
      printf("p - print list\n");
      printf("s number - search for any given number in list\n");
      printf("d number - deletes a number in list\n");
      printf("x - exit program\n");
    }
}
}
