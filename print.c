#include "main.h"

//This function will print the data in the linked list, in order, separated by a single space
void print(struct node*ll){
int current=ll[0].next;
  while(current != MYNULL){ //loop through list until reaching MYNULL
    printf("%d ",ll[current].data); //prints data followed by a space
    current=ll[current].next;
  }
printf("\n");
}
