#include "main.h"

//This function loops through nodes in list to verify it exists
//Will enable main to print:
//FOUND - if node is in list
//NOT FOUND - if node does not exist
int search(struct node*ll, int num)
{
int current=ll[0].next; //where sentinel node points
  while(current != MYNULL){
      if(ll[current].data == num) return(1);
        current=ll[current].next; //loop through list
    }
    if(current == MYNULL) return (0);//node does not exist
  }
