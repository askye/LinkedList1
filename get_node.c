#include "main.h"

//Function will find the next available node in list
//returns the index of available node, or MYNULL if list is empty
int get_node(struct node*ll)
{
  for(int i=1; i<=99; i++){ //loop through list until empty node is found
    if(ll[i].valid==0){
      ll[i].valid=1; //set node to valid 1
      ll[i].next=MYNULL; //node pointing to MYNULL
      return(i); //returns index of node
    }
  }
return(MYNULL); //if list is empty
}
