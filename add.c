#include "main.h"

//will insert the given number into the list, in order, provided the number is not already in the list
//function will enable main to print:
//SUCCESS - if data is added successfully
//OUT OF SPACE - if there is not an available node in list
//NODE ALREADY IN LIST - if node already exists

int add(struct node*ll,int num)
{
int prev=0; //sentinel
int curr=ll[prev].next; //where sentinel is pointing
int new=get_node(ll); //get new index
int existN=search(ll,num); //see if num already exists
//if num is not in list, continue with add
  if(existN == 0){
    if(new == MYNULL){ //list is out of space
      printf("OUT OF SPACE\n");
      return(0); //do not add num
    }
    if(new != MYNULL){ //if node exists
      while(curr != MYNULL && num>ll[curr].data){ //loop through to find
        prev=curr;				//correct node space
        curr=ll[curr].next;
      }
      ll[new].data=num; //add num to linked list
      int temp=ll[prev].next;
      ll[prev].next=new;
      ll[new].next=temp;
      return(1);//successful add
  }
}
else{
  printf("NODE ALREADY IN LIST\n");
  return(0); //node already exists
}
}
