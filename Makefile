main: main.o init.o add.o print.o delete.o search.o get_node.o release_node.o
	gcc -o main main.o init.o add.o print.o delete.o search.o get_node.o release_node.o
main.o: main.c main.h
	gcc -c main.c
init.o: init.c
	gcc -c init.c
add.o: add.c
	gcc -c add.c
print.o: print.c
	gcc -c print.c
delete.o: delete.c
	gcc -c delete.c
search.o: search.c
	gcc -c search.c
get_node.o: get_node.c
	gcc -c get_node.c
release_node.o: release_node.c
	gcc -c release_node.c
clean:
	rm main main.o init.o add.o print.o delete.o search.o get_node.o release_node.o
