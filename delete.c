#include "main.h"

//This function will delete a specified node in list
//Will enable main to print:
//SUCCESS - if node is successfully deleted
//NODE NOT FOUND - if node does not exist
int delete(struct node*ll, int num)
{
int prev=0;
int curr=ll[0].next;
  while(curr != MYNULL && ll[curr].data != num){
    prev=curr;
    curr=ll[curr].next; //will move through list until data matches num
  }
    if(ll[curr].data == num){  
      ll[prev].next=ll[curr].next; //set prev.next to look at curr.next
      ll[curr].next=MYNULL; //set curr.next to MYNULL
      release_node(ll,curr); //delete node from list
      return(1); //success
    }
   else{
     return(0);
   }
}
