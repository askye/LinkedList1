#include <stdio.h>
#define MYNULL 0

struct node{
  int data;
  int next;
  int valid;
};

void init(struct node*); //initialize
int add(struct node*, int); //add node
void print(struct node*); //print list
int delete(struct node*,int); //delete node
int search(struct node*, int); //search for data
int get_node(struct node*); //search for empty node
void release_node(struct node*, int); //clears valid flag after deleting node

